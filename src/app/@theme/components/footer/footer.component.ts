import { Component } from '@angular/core';

@Component({
  selector: 'app-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Criado com ♥ por <b>ALGUÉM</b> {{data.getFullYear()}}</span>
  `,
})
export class FooterComponent {
  public data: Date = new Date();
}
